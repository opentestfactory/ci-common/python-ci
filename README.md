<!--

     Copyright (c) 2020 - 2021 Henix, henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->

# python-ci

## Use this shared python pipeline in your project

### > Define your project's gitlab-ci

This repository contains the gitlab-ci pipeline shared between otf python components.

Your project will use this shared configuration by importing `gitlab-ci-python.yml` in its own `.gilab-ci.yml`:

``` yaml
include:
  - project: 'opentestfactory/ci-common/python-ci'
    ref: main
    file: 'gitlab-ci-python.yml'
```

> **Note:** By default this pipeline pushes the released wheel our internal nexus. See bellow for pushing to PyPi.

### > Add a sonar-project.properties

Your project should also add a `sonar-project.properties` file in its repository based on the `sonar-project.template.properties` provided in this project.
This template should be customized by replacing `<project-key>` and `<project name>` according your project name (There's an example inside)

## Pushing release to pypi

For pushing your released wheel to pypi you have to override the pipeline variable `PUBLISH_RELEASE_ON_PYPI` in the gitlab-ci.yml of your project like in this example:

``` yaml
include:
  - project: 'opentestfactory/ci-common/python-ci'
    ref: main
    file: 'gitlab-ci-python.yml'

variables:
  PUBLISH_RELEASE_ON_PYPI: "true"
```

To ensure the package is valid, the package is pushed in the `test.pypi.org` repository. Job: `pypi-classifier-check`
> <details><summary>Technical choices</summary>
>  It pushes the package on the test.pypi repository through the user: opentestfactory_testpypi <br>
>  -  test.pypi gives no guaranty on it's db, therefore the job is on allow_failure=true.<br>
>  -  Re-upload is forbidden on the repo, therefore a tag is added on the test job.
> </details>

## Specific configuration for unit test

If your project has tests to run in the CI, it should:

* Uncomment in your `sonar-project.properties` file the line `#sonar.tests=tests/python`
* Add the following lines in the .gilab-ci.yml file to execute the project tests:
  ```
  pip install unittest-xml-reporting coverage -e .
  coverage run --source=opentf -m xmlrunner discover -s tests/python && coverage xml
  ```

## Gitlab-ci variables

When you use this gitlab-ci-python.yml in your project, the CI execution will the variable bellow (in the project or from the groups):

* Python Nexus
  * PIP_INDEX_URL
    * https://`<user>`:`<password>`@`<nexus-pypi-group>`/simple
  * PYPI_INTERNAL_RELEASE_REPO_URL
  * PYPI_INTERNAL_ACCEPTANCE_REPO_URL
  * PYPI_INTERNAL_TEMP_REPO_URL
  * PYPI_INTERNAL_REPO_PASSWORD
  * PYPI_INTERNAL_REPO_USERNAME

* PyPi official
  * PYPI_PROD_REPO_PASSWORD
  * PYPI_PROD_REPO_USERNAME

* PyPi test
  * PYPI_TEST_REPO_PASSWORD
  * PYPI_TEST_REPO_USERNAME

* Sonarcloud
  * SONAR_TOKEN
  * SONAR_HOST_URL
  * SONARCLOUD_ORGANIZATION
